Documentation for running the MPI Programs
==========================================

If you have not done so already, make sure to run the following:

	module load impi

This will load in the needed Intel MPI tools.

The Hello World MPI Program
===========================
		
	For hello_mpi.c:
		
		1. Compile the program
			mpicc -o hello_mpi hello_mpi.c
			
		2. Run the program.
			sbatch hello_mpi.sh
			
The Matrix Multiplication Program
=================================

	Introductory Note
	
		This matrix multiplication program was written
		for Dr. Karavanic's CS 510 course Introduction to Performance. The
		program is designed to use MPI for distributed memory multiplication
		of dense matrices. The datatype used in the matrices are 4-byte integers,
		but in practice we could swap this out for another data type if
		we like.

	For matmul_mpi.c:

		1. Compile the program
			mpicc -std=c99 -o matmul_mpi matmul_mpi.c

			a) We can compile the program so that each compute node greets the user
			by indicating that a given process is starting and ending.

				mpicc -std=c99 -o matmul_mpi matmul_mpi.c -DGREET
			
			b) We can compile the program so that each compute node prints its portion
			of the matrices it holds in memory (recommend for matrices of size at most
			16x16, otherwise the screen is just a giant text dump).

				mpicc -std=c99 -o matmul_mpi matmul_mpi.c -DPRINT

			c) We can also do a combination of the above.

				mpicc -std=c99 -o matmul_mpi matmul_mpi.c -DGREET -DPRINT
			
		2. Run the program.
			sbatch matmul_mpi.sh
			
		3. If you want to set the shell script to multiply two different types
		of matrices, I suggest modifying the matmul_mpi.sh script as follows:
		
			./matmul_mpi R N
				-Multiplies two random matrices of size NxN
				
			./matmul_mpi S N
				-Multiplies a matrix A, with A[i][j] = i + j, by a matrix of 1s.
				
			./matmul_mpi L N
				-Multiplies 1D finite difference laplacian (sans the factor
				of 1/h^2) by a matrix of 1s.
				
#-----------------------------------------------------------------------------#






























