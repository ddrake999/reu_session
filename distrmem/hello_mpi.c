#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[])
{
    int rank, world_size;
    char processor[MPI_MAX_PROCESSOR_NAME];
    int name_len;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Get_processor_name(processor, &name_len);

    printf("Hello world from rank %2d of %2d rank(s) on processor %s!\n", rank, world_size, processor);

    MPI_Finalize();

    return 0;
}
