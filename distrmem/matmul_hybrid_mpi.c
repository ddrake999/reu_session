/*
 * matmul_mpi.c
 *
 * Here, we'll start with a simple approach.
 *
 * Our program will take a command line argument indicating the size
 * of the matrix. Our big assumption is that the number of processes
 * is a value less than or equal to the dimension of the matrix. This
 * means with N rows in the matrix, we can have at most N processes.
 * Otherwise, we'll quit. In other words, every process takes some
 * number of rows in the matrix, so each chunk spans the entire set 
 * of columns for the matrix.
 *
 * As a consequence, each matrix will get an (N/p) x N chunk of the
 * first two input matrices, and is thus responsible for setting P
 * (N/P) x (N/P) chunks in the output matrix on a given block row.
 *
 * That is, if process p has the row chunk (ip, (i+1)p) x (0, N - 1),
 * then process will fill up all (N/P) x (N/P) chunks between rows
 * ip and (i + 1)p between columns jp and (j + 1)p for j in 0, 1, ..., p - 1.
 * NOTE: May need to fix the above explanation. 
 *
 * The multiplication will be done by passing around the columns in the 
 * second matrix in a ring to all of the processes, thereby allowing each
 * process to fill in the columns and rows designated to it.
 *
 * Next, process 0 will allocate two (dense) matrices of size NxN, and the distribute their
 * respective row and column chunks to the other processes.
 *
 * best performance on 1024x1024 is with OMP_NUM_THREADS=4, -np 16, --map-by ppr:1:node
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <math.h>
#include <limits.h>
#include <time.h>

#define RING_TAG 1000
#define MIN(a, b) ((a) < (b)) ? (a) : (b);

//
// A structure containing index data for the array chunks we'll pass around to
// the other processes.
//
typedef struct matrix_chunk_index_data_s
{
	int capacity;
	int local_rows, local_cols;
	int global_rows, global_cols;
	int starti, endi;
	int startj, endj;
} matrix_chunk_index_data_t;

//
// The matrix chunk itself, which contains the raw numerical data and index
// data about the chunk.
//
typedef struct matrix_chunk_s
{
	int *data;
	
	struct matrix_chunk_index_data_s index_data;
} matrix_chunk_t;

// Initializes the matrix chunk, return 0 on success, 1 on failure.
// By default, all entries in the matrix chunk are set to zero.
int mc_init(matrix_chunk_t *, int, int, int, int, int, int, int);

// Function for setting all entries in a matrix chunk to a specific value.
void mc_set(matrix_chunk_t *, int);

// Function for setting all entries in a matrix in a chunk to correspond
// to the 2D finite difference laplacian (sans the inverse of the square
// of the step size h).
void mc_laplacian(matrix_chunk_t *);

// Function that initializes an identity matrix (in pieces) across all processes.
void mc_identity(matrix_chunk_t *);

// Function that initializes the ijth entry to i + j, 0 <= i, j <= N, where
// N is one dimension of the matrix.
void mc_sequence(matrix_chunk_t *);

// Function that initializes the ijth entry to a ranomd integer between 0 and N - 1, inclusive.
void mc_random(matrix_chunk_t *, int);

// Function that frees a matrix chunk that is no longer in use.
void mc_free(matrix_chunk_t *);

// Function that prints the array chunks from the various processes.
void ring_print(const matrix_chunk_t *, int, int);

// Function that prints a chunk of the matrix.
void chunk_print(const matrix_chunk_t *, int, int);

// Function that multiplies two array chunks A and B together, and then
// forms a new one.
void chunk_multiply(matrix_chunk_t *, matrix_chunk_t *, matrix_chunk_t *);

// Copies the matrix chunk on the right into the matrix chunk on the left.
void chunk_copy(matrix_chunk_t *, const matrix_chunk_t *);

// Prints the data in a chunk.
void chunk_debug_print(const matrix_chunk_t *, const char *, int, int);

int main(int argc, char *argv[])
{
	int rank, world_size;					// Rank of the current process and world size.
	int dimension;							// The row and column dimension of the matrices well
	long parsed_command_line_dimension;		// The parsed command line argument provided by the user.
	
	// The processor name and the name length.
	char processor[MPI_MAX_PROCESSOR_NAME];
	int processor_name_len;
	
	// Variables uses for timing the actual matrix-matrix multiplication.
	double matmul_start, matmul_end;
	double matmul_min_start, matmul_max_end;
	double matmul_elapsed, max_matmul_elapsed;
	
	// Variables used for timing the entirety of the program, from right after
	// the MPI_Init() call to right before the MPI_Finalize() call.
	double process_start, process_end;
	double process_min_start, process_max_end;
	double process_elapsed, max_process_elapsed;
	
	// The threading level we desire and 
	// what is actually provided, respectively.
	// The threading we desire comes in four flavors:
	//
	// MPI_THREAD_SINGLE     - No multi-threading is to be done.
	// MPI_THREAD_FUNNELED   - Only the thread that called MPI_Init_thread() can make MPI calls.
	// MPI_THREAD_SERIALIZED - Only one thread can make an MPI call at a time.
	// MPI_THREAD_MULTIPLE   - Multiple threads can make MPI calls -> This is unsupported in the linux labs,
	//                         as per calling ompi_info | grep -i thread
	//
	// When calling the MPI_Thread_init with MPI_THREAD_MULTIPLE as required, we
	// end up getting MPI_THREAD_SERIALIZED by default, so only one thread can
	// call an MPI routine in a parallel region at a time in the Linux Lab machines.
	int required = MPI_THREAD_FUNNELED;
	int provided;
	
	// The chunks of the first and second matrices we will multiply together.
	matrix_chunk_t A, B;
	
	// The resulting chunk that this given rank computes.
	matrix_chunk_t C;
	
	MPI_Init_thread(&argc, &argv, required, &provided);
	
	// Start timing the program here.
	process_start = MPI_Wtime();
	
	// Get the world size.
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	
	// Get the processor name.
	MPI_Get_processor_name(processor, &processor_name_len);
	
#ifdef PRINT
	// Have rank 0 print out the threading support.
	if (rank == 0) {
		switch (provided) {
			case MPI_THREAD_SINGLE:
				printf("Rank 0: Provided thread support is MPI_THREAD_SINGLE.\n");
				
				break;
			case MPI_THREAD_FUNNELED:
				printf("Rank 0: Provided thread support is MPI_THREAD_FUNNELED.\n");
				
				break;
			case MPI_THREAD_SERIALIZED:
				printf("Rank 0: Provided thread support is MPI_THREAD_SERIALIZED.\n");
				
				break;
			case MPI_THREAD_MULTIPLE:
				printf("Rank 0: Provided thread support is MPI_THREAD_MULTIPLE.\n");
				
				break;
		}
	}
#endif

#ifdef GREET	
	printf("Rank %d: Starting on processor %s.\n", rank, processor);
#endif

	// Get the matrix dimension from the command line. Every rank participates.
	if (argc < 2) {
		// Have rank 0 whine to the user.
		if (rank == 0) {
			fprintf(stderr, "Rank %d: Usage \n\t./matmul_mpi <N> <C>, where <N> is the number of rows and columns in the matrix and <C> is a character indicating the initialization type.\n", rank);
		}
		
		MPI_Finalize();
		exit(EXIT_FAILURE);
	}
	
	parsed_command_line_dimension = strtol(argv[1], NULL, 10);
	
	if (parsed_command_line_dimension <= 0) {
		// Have rank 0 whine to the user.
		if (rank == 0) {
			fprintf(stderr, "Rank %d: The dimension specified is must be a positive integer.\n", rank);
		}
		
		MPI_Finalize();
		exit(EXIT_FAILURE);
	} else if (parsed_command_line_dimension > INT_MAX) {
		// Have rank 0 whine to the user.
		if (rank == 0) {
			fprintf(stderr, "Rank %d: The dimension specified is greater than the largest signed integer %d.\n", rank, INT_MAX);
		}
		
		MPI_Finalize();
		exit(EXIT_FAILURE);
	}
	
	// Now set the dimension of the matrices we'll be multiplying.
	dimension = parsed_command_line_dimension;
	
	// If the world size exceeds the dimension of the matrices whose products we are computing, exit.
	if (world_size > dimension) {
		// Have rank 0 whine to the user.
		if (rank == 0) {
			fprintf(stderr, "Rank %d: There are more process specified than rows in the matrices.\n", rank);
		}
		
		MPI_Finalize();
		exit(EXIT_FAILURE);
	}
	
	// Compute the number of rows and columns per process.
	int rows_per_process = dimension / world_size;
	int cols_per_process = dimension / world_size;
	
	// Now have each array allocate its first and second chunks.
	int starti, endi;
	int startj, endj;
	
	// Set the starting ith row index.
	starti = rank * rows_per_process;
	
	// Set the starting and ending jth column indices. For this
	// algorithm, we always go from the beginning of a given
	// row to the end.
	startj = 0;
	endj = dimension - 1;
	
	// The ending ith row index is set depending on the rank. In particular,
	// this is designed so that the last rank gets more rows to work on if
	// the number of processes does not evenly divide the dimension.
	if (rank < world_size - 1) {
		endi = (rank + 1) * rows_per_process - 1;
	} else {
		endi = dimension - 1;
	}
	
	// The next thing we'll do is compute the maximum chunk size.
	int local_chunk_size = (endi - starti + 1) * (endj - startj + 1);
	int max_chunk_size;
	
	// Have each process know the maximum chunk size.
	MPI_Allreduce(&local_chunk_size, &max_chunk_size, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);

#ifdef PRINT
	if (rank == 0) {
		printf("Rank %d: Maximum chunk size is %d elements, %lu bytes, %.2e KB, %.2e MB, %.2e GB\n", rank, max_chunk_size, 
																									max_chunk_size * sizeof(int),
																									(double)(max_chunk_size * sizeof(int)) / pow(2, 10),
																									(double)(max_chunk_size * sizeof(int)) / pow(2, 20),
																									(double)(max_chunk_size * sizeof(int)) / pow(2, 30));
	}
#endif
	
	// Initialize the first matrix in the multiplication.
	if (0 != mc_init(&A, max_chunk_size, dimension, dimension, starti, endi, startj, endj)) {
		fprintf(stderr, "Rank %d: Could not allocate memory for first matrix chunk on processor %s.\n", rank, processor);
		
		MPI_Finalize();
		
		exit(EXIT_FAILURE);
	}
	
	// Remember that the matrix B will have the corresponding transposed indices of A during the multiplication.
	if (0 != mc_init(&B, max_chunk_size, dimension, dimension, startj, endj, starti, endi)) {
		fprintf(stderr, "Rank %d: Could not allocate memory for second matrix chunk on processor %s.\n", rank, processor);
		
		mc_free(&A);
		MPI_Finalize();
		
		exit(EXIT_FAILURE);
	}
	
	// Finally, C will be an max((N/P), N - (P-1)*N/P) x N chunk, where N is the row and column dimension
	// of the matrix and P is the number of processors (the world_size variable).
	if (0 != mc_init(&C, max_chunk_size, dimension, dimension, starti, endi, startj, endj)) {
		fprintf(stderr, "Rank %d: Could not allocate memory for output matrix chunk on processor %s.\n", rank, processor);
		
		mc_free(&B);
		mc_free(&A);
		MPI_Finalize();
		
		exit(EXIT_FAILURE);
	}
	
	// Initialize the matrices A and B based on the user input.
	char user_input_choice = argc >= 3 ? *argv[2] : 'L';
	
	// Seed for the random number generator.
	int seed;
	
	switch (user_input_choice)
	{
		case 'R':
		case 'r':
			mc_random(&A, time(NULL) + rank);
			mc_random(&B, time(NULL) + world_size - rank);
			
			break;
			
		case 'S':
		case 's':
			mc_sequence(&A);
			mc_set(&B, 1);
			
			break;
			
		default:
			mc_laplacian(&A);
			mc_set(&B, 1);
			
			break;
	}
	
	// Now set the output matrix.
	mc_set(&C, 0);

	
#ifdef PRINT
	ring_print(&A, rank, world_size);
	ring_print(&B, rank, world_size);
	ring_print(&C, rank, world_size);
#endif

	// If there is only one process, we can do the matrix multiplication right away and exit.
	if (world_size == 1) {
		// Multiply the chunks.
		matmul_start = MPI_Wtime();
		chunk_multiply(&C, &A, &B);
		matmul_end = MPI_Wtime();

#ifdef PRINT
		// Print the resulting matrix.
		ring_print(&C, rank, world_size);
#endif
		
		// Free the memory chunks we no longer need.
		mc_free(&A);
		mc_free(&B);
		mc_free(&C);
		
#ifdef GREET
		printf("Rank %d: Ending on processor %s.\n", rank, processor);
#endif

		// Get the ending time.
		process_end = MPI_Wtime();
		
		MPI_Finalize();
		
		// Now print the elapsed time.
		printf("Rank %d: Matrix Multiplication Elapsed Time (Start of First Process to End of the Last Process): %lf\n", rank, matmul_end - matmul_start);
		printf("Rank %d: Process Elapsed Time (Start of First Process to End of the Last Process): %lf\n", rank, process_end - process_start); 
		
		exit(EXIT_SUCCESS);
	}
	
	//
	// Now we'll do the actual matrix-vector multiplication. Our algorithm
	// proceeds as follows:
	//
	// Begin by having each chunk do its matrix-vector multiplication on
	// each chunk. 
	//
	// First, the ith process takes its ith row chunk and multiplies by its
	// jth column chunk, thus filling the ijth chunk in its output array.
	//
	// Next, the ith rank sends its jth column chunk to process i + 1, while
	// simultaneously receiving the jth row chunk from process i - 1. We
	// then fill in the i(j+1)th chunk in this process' output array through
	// serial matrix-vector multiplication.
	//
	// We loop up until the entirety of the world size, and then display the results.
	//
	// In effect, this algorithm leaves the Pth process responsible for P many
	// (N/P) x (N/P) chunks to set in its row chunk.
	
	//
	// First, we need to do something very MPI-centric: I want to pass an arbitrary
	// blob of bytes (i.e., a struct) over the wire. What we'll need to do is create
	// a user-defined type that MPI can digest.
	//
	// More at: 
	//    https://www.rc.colorado.edu/sites/default/files/Datatypes.pdf
	//    http://pages.tacc.utexas.edu/~eijkhout/pcse/html/mpi-data.html
	//    http://www.netlib.org/utk/papers/mpi-book/node72.html
	//    http://www.mcs.anl.gov/research/projects/mpi/mpi-standard/mpi-report-1.1/node80.htm
	//	  https://stackoverflow.com/questions/20064083/creating-new-drive-datatype-from-struct-type-in-mpi
	
	//
	// We will be creating this datatype for the struct of metadata about the chunk's local
	// indices and global indices
	//
	
	// The number of blocks in the struct.
	int count = 9;
	
	// The size of each block.
	int blocks[9];
	
	// The displacements of each member in the struct.
	MPI_Aint displacements[9];
	MPI_Aint dispint;
	
	// Get the displacements for an MPI_INT.
	MPI_Type_extent(MPI_INT, &dispint);
	
	// The types of each block.
	MPI_Datatype types[9];
	
	// Initialize the block, displacement, and type information.
	for (int i = 0; i < count; i++) {
		blocks[i] = 1;
		displacements[i] = i * dispint;
		types[i] = MPI_INT;
	}
	
	// The datatype we are defining.
	MPI_Datatype MATRIX_CHUNK_INDEX_DATA;
	
	// Create the type we can use to pass around the data.
	MPI_Type_struct(count, blocks, displacements, types, &MATRIX_CHUNK_INDEX_DATA);
	MPI_Type_commit(&MATRIX_CHUNK_INDEX_DATA);
	
	//
	// Now that we've committed the type, the next step is to do the actual matrix-vector multiplication.
	// We will do so in the spirit of the MPI Ring program: http://mpitutorial.com/tutorials/mpi-send-and-receive/
	//
	
	// Create the chunks into which we are sending and receiving.
	matrix_chunk_t send_chunk, recv_chunk;
	
	// Initialize the send and receive chunks. In this case, the chunks we are
	// sending and receiving are all columns chunks passed around from column
	// chunks in the second matrix.
	mc_init(&send_chunk, max_chunk_size, dimension, dimension, 0, 0, 0, 0);
	mc_init(&recv_chunk, max_chunk_size, dimension, dimension, 0, 0, 0, 0);
	
	// The previous and next ranks to recieve and send from, respectively.
	int source, destination;
	
	// The status of the resulting MPI_Sendrecv call.
	MPI_Status sendrecv_status;
	
	// Start timing the matrix-matrix multiplication.
	matmul_start = MPI_Wtime();
	
	for (int i = 0; i < world_size; i++) {
		// Perform the matrix-vector multiplication.
		chunk_multiply(&C, &A, &B);
		
		// Copy the matrix chunk in B to that of the chunk we're receiving.
		chunk_copy(&send_chunk, &B);
		
		// Specify the previous and next ranks.
		source = rank == 0 ? world_size - 1 : rank - 1;
		destination = (rank + 1) % world_size;
		
		// Now perform the send-recv call with the previous and next ranks.
		//
		// I see the bug. MPI is copying pointers, and in the case of 1 rank,
		// it is overwriting the point from the send chunk into the receive
		// chunk, hence the error. Instead, what we'll do is this: We'll
		// do a send-recv of the array data over the wire, followed by the
		// chunk index data. Easy peasy.
		
		// Perform a send-recv on the actual raw data in the matrix.
		MPI_Sendrecv(
			send_chunk.data, max_chunk_size, MPI_INT, destination, RING_TAG, 
			recv_chunk.data, max_chunk_size, MPI_INT, source, RING_TAG,
			MPI_COMM_WORLD, &sendrecv_status
		);
		
		// Now send and receive index information over the wire.
		MPI_Sendrecv(
			&send_chunk.index_data, 1, MATRIX_CHUNK_INDEX_DATA, destination, RING_TAG, 
			&recv_chunk.index_data, 1, MATRIX_CHUNK_INDEX_DATA, source, RING_TAG,
			MPI_COMM_WORLD, &sendrecv_status
		);
		
		// Copy the received chunk into B.
		chunk_copy(&B, &recv_chunk);
	}
	
	// Get the ending time for the matrix multiplication.
	matmul_end = MPI_Wtime();
	
#ifdef PRINT
	// To confirm our result, do a ring print of the resulting matrix.
	ring_print(&C, rank, world_size);
#endif
	
	// Free the memory chunks we no longer need.
	mc_free(&A);
	mc_free(&B);
	mc_free(&C);
	
	// Free the send and receive chunks we created.
	mc_free(&send_chunk);
	mc_free(&recv_chunk);
	
	// Free the chunk type we are no longer using.
	MPI_Type_free(&MATRIX_CHUNK_INDEX_DATA);

#ifdef GREET	
	printf("Rank %d: Ending on processor %s.\n", rank, processor);
#endif
	
	// Finish timing the program here.
	process_end = MPI_Wtime();
	
	// Collect the minimum and maximum start times for the matrix-matrix multiplications and processes.
	MPI_Reduce(&matmul_start, &matmul_min_start, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&matmul_end, &matmul_max_end, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	MPI_Reduce(&process_start, &process_min_start, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
	MPI_Reduce(&process_end, &process_max_end, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
	
	MPI_Finalize();
	
	// Now print the elapsed time.
	if (rank == 0) {
		printf("Matrix Multiplication Elapsed Time (Start of First Process to End of the Last Process): %lf\n", matmul_max_end - matmul_min_start);
		printf("Process Elapsed Time (Start of First Process to End of the Last Process): %lf\n", process_max_end - process_min_start); 
	}
	
	return 0;
}

int mc_init(matrix_chunk_t *chunk, int capacity, int global_rows, int global_cols, int starti, int endi, int startj, int endj)
{
	int success_flag = 0;
	
	chunk->index_data.capacity = capacity;
	
	chunk->index_data.global_rows = global_rows;
	chunk->index_data.global_cols = global_cols;
	
	chunk->index_data.local_rows = endi - starti + 1;
	chunk->index_data.local_cols = endj - startj + 1;
	
	chunk->index_data.starti = starti;
	chunk->index_data.endi = endi;
	chunk->index_data.startj = startj;
	chunk->index_data.endj = endj;
	
	// Allocate the matrix as a single, contiguous array.
	chunk->data = calloc(capacity, sizeof(int));
	
	if (NULL == chunk->data) {
		success_flag = 1;
		
		chunk->index_data.capacity = 0;
		chunk->index_data.global_rows = chunk->index_data.global_cols = 0;
		chunk->index_data.local_rows = chunk->index_data.local_cols = 0;
		chunk->index_data.starti = chunk->index_data.endi = 0;
		chunk->index_data.startj = chunk->index_data.endj = 0;
	}
	
	return success_flag;
}

void mc_set(matrix_chunk_t *chunk, int value)
{
	for (int k = 0; k < chunk->index_data.local_rows * chunk->index_data.local_cols; k++) {
		chunk->data[k] = value;
	}
}

void mc_laplacian(matrix_chunk_t *chunk)
{
	int min_dimension = MIN(chunk->index_data.local_rows, chunk->index_data.local_cols);
	int index = chunk->index_data.starti;
	
	// If we're starting in a chunk away from the left
	// edge, set the subdiagonal entry on the left.
	if (chunk->index_data.starti > 0) {
		chunk->data[index - 1] = -1;
	}
	
	for (int i = 0; i < min_dimension; i++) {
		// Set the corresponding index of the entry in the array.
		index = i * chunk->index_data.local_cols + (chunk->index_data.starti + i);
		
		// Set the diagonal and subdiagonal entries.
		chunk->data[index] = 2;
		
		// Set the subdiagonal entry.
		if (i < min_dimension - 1) {
			chunk->data[index + chunk->index_data.local_cols] = -1;
		}
		
		// Set the superdiagonal entry.
		if (chunk->index_data.starti + i < chunk->index_data.global_cols - 1) {
			chunk->data[index + 1] = -1;
		}
	}
}

// Function that initializes an identity matrix (in pieces) across all processes.
void mc_identity(matrix_chunk_t *chunk)
{
	int min_dimension = MIN(chunk->index_data.local_rows, chunk->index_data.local_cols);
	int index;
	
	for (int i = 0; i < min_dimension; i++) {
		index = i * chunk->index_data.local_cols + (chunk->index_data.starti + i);
		
		chunk->data[index] = 1;
	}
}

// Function that initializes the ijth entry to i + j, 0 <= i, j <= N, where
// N is one dimension of the matrix.
void mc_sequence(matrix_chunk_t *chunk)
{
	int index;
	
	for (int i = 0; i < chunk->index_data.local_rows; i++) {
		for (int j = 0; j < chunk->index_data.local_cols; j++) {
			index = i * chunk->index_data.local_cols + j;
			
			chunk->data[index] = chunk->index_data.starti + i + chunk->index_data.startj + j;
		}
	}
}

// Function that initializes the ijth entry to a ranomd integer between 0 and N - 1, inclusive.
void mc_random(matrix_chunk_t *chunk, int seed)
{
	int index;
	int N = MIN(chunk->index_data.global_rows, chunk->index_data.global_cols);
	
	// Seed the random number generator.
	srand(seed);
	
	// Set the matrix entries.
	for (int i = 0; i < chunk->index_data.local_rows; i++) {
		for (int j = 0; j < chunk->index_data.local_cols; j++) {
			index = i * chunk->index_data.local_cols + j;
			
			chunk->data[index] = rand() % N;
		}
	}
}

void mc_free(matrix_chunk_t *chunk)
{
	free(chunk->data);
	chunk->data = NULL;
	
	chunk->index_data.capacity = 0;
	chunk->index_data.starti = chunk->index_data.endi = 0;
	chunk->index_data.startj = chunk->index_data.endj = 0;
	chunk->index_data.local_rows = chunk->index_data.local_cols = 0;
	chunk->index_data.global_rows = chunk->index_data.global_cols = 0;
}

// Function that prints the array chunks from the various processes.
void ring_print(const matrix_chunk_t *chunk, int rank, int world_size)
{
	int ring_flag;	// A flag we pass to successive processes so they know it's their turn to print.
	
	if (rank == 0) {
		// Set the ring flag as the first process.
		ring_flag = 0;
		
		// Print the first chunk.
		chunk_print(chunk, rank, world_size);
		
		// If the world size is greater than 1, send the flag to the next process.
		if (world_size > 1) {
			MPI_Send(&ring_flag, 1, MPI_INT, rank + 1, RING_TAG, MPI_COMM_WORLD);
			
			// Now receive from the list process.
			MPI_Recv(&ring_flag, 1, MPI_INT, world_size - 1, RING_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			
			printf("Rank %d: Printing is done.\n", rank);
		}
	} else {
		// Receive from the previous rank and increment the flag variable.
		MPI_Recv(&ring_flag, 1, MPI_INT, rank - 1, RING_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		
		// Increment the flag variable.
		ring_flag++;
		
		// Print the current rank's chunk.
		chunk_print(chunk, rank, world_size);
		
		// Send the flag variable to the next rank.
		MPI_Send(&ring_flag, 1, MPI_INT, (rank + 1) % world_size, RING_TAG, MPI_COMM_WORLD);
	}
	
}

// Function that prints a chunk of the matrix.
void chunk_print(const matrix_chunk_t *chunk, int rank, int world_size)
{
	int index;
	
	// Print the current rank's chunk.
	for (int i = 0; i < chunk->index_data.local_rows; i++) {
		printf("Rank %4d: ", rank);
		
		for (int j = 0; j < chunk->index_data.local_cols; j++) {
			index = i * chunk->index_data.local_cols + j;
			
			printf("%4d ", chunk->data[index]);
		}
		
		printf("\n");
	}
	
	printf("\n");
}

// Function that multiplies two array chunks A and B together, and then
// forms a new one.
void chunk_multiply(matrix_chunk_t *C, matrix_chunk_t *A, matrix_chunk_t *B)
{
	int M = A->index_data.local_rows;
	int N = B->index_data.local_cols;
	int P = A->index_data.local_cols;
	
	int cstarti = 0;
	int cendi = A->index_data.local_rows - 1;
	
	int cstartj = B->index_data.startj;
	int cendj = B->index_data.endj;
	
	int aindex;
	int bindex;
	int cindex;
	
	#pragma omp parallel for	shared(A, B, C, M, N, P, cstarti, cendi, cstartj, cendj) \
								schedule(static) \
								collapse(3)
	for (int i = cstarti; i <= cendi; i++) {
		for (int j = cstartj; j <= cendj; j++) {
			for (int k = 0; k < P; k++) {
				C->data[i * C->index_data.local_cols + j] += A->data[i * A->index_data.local_cols + k] * 
																B->data[k * B->index_data.local_cols + (j - cstartj)];
			}
		}
	}
	
}

// Copies the matrix chunk on the right into the matrix chunk on the left.
void chunk_copy(matrix_chunk_t *dst, const matrix_chunk_t *src)
{
	dst->index_data.capacity = src->index_data.capacity;
	
	dst->index_data.global_rows = src->index_data.global_rows;
	dst->index_data.global_cols = src->index_data.global_cols;
	
	dst->index_data.local_rows = src->index_data.endi - src->index_data.starti + 1;
	dst->index_data.local_cols = src->index_data.endj - src->index_data.startj + 1;
	
	dst->index_data.starti = src->index_data.starti;
	dst->index_data.endi = src->index_data.endi;
	dst->index_data.startj = src->index_data.startj;
	dst->index_data.endj = src->index_data.endj;
	
	// Allocate the matrix as a single, contiguous array.
	memmove(dst->data, src->data, src->index_data.capacity * sizeof(int));
}

// Prints the data in a chunk.
void chunk_debug_print(const matrix_chunk_t *chunk, const char *name, int print_array, int rank)
{
	if (name != NULL) {
		printf("Rank %5d: %s->index_data.capacity    = %d\n", rank, name, chunk->index_data.capacity);
		printf("Rank %5d: %s->index_data.global_rows = %d\n", rank, name, chunk->index_data.global_rows);
		printf("Rank %5d: %s->index_data.global_cols = %d\n", rank, name, chunk->index_data.global_cols);
		printf("Rank %5d: %s->index_data.local_rows  = %d\n", rank, name, chunk->index_data.local_rows);
		printf("Rank %5d: %s->index_data.local_cols  = %d\n", rank, name, chunk->index_data.local_cols);
		printf("Rank %5d: %s->index_data.starti      = %d\n", rank, name, chunk->index_data.starti);
		printf("Rank %5d: %s->index_data.endi        = %d\n", rank, name, chunk->index_data.endi);
		printf("Rank %5d: %s->index_data.startj      = %d\n", rank, name, chunk->index_data.startj);
		printf("Rank %5d: %s->index_data.endj        = %d\n", rank, name, chunk->index_data.endj);
		
		if (print_array) {	
			printf("Rank %5d: %s->data    = ", rank, name);
			
			for (int k = 0; k < chunk->index_data.capacity; k++) {
				printf("%d ", chunk->data[k]);
			}
			
			printf("\n");
		}
	} else {
		printf("Rank %5d: chunk->index_data.capacity    = %d\n", rank, chunk->index_data.capacity);
		printf("Rank %5d: chunk->index_data.global_rows = %d\n", rank, chunk->index_data.global_rows);
		printf("Rank %5d: chunk->index_data.global_cols = %d\n", rank, chunk->index_data.global_cols);
		printf("Rank %5d: chunk->index_data.local_rows  = %d\n", rank, chunk->index_data.local_rows);
		printf("Rank %5d: chunk->index_data.local_cols  = %d\n", rank, chunk->index_data.local_cols);
		printf("Rank %5d: chunk->index_data.starti      = %d\n", rank, chunk->index_data.starti);
		printf("Rank %5d: chunk->index_data.endi        = %d\n", rank, chunk->index_data.endi);
		printf("Rank %5d: chunk->index_data.startj      = %d\n", rank, chunk->index_data.startj);
		printf("Rank %5d: chunk->index_data.endj        = %d\n", rank, chunk->index_data.endj);
		
		if (print_array) {	
			printf("Rank %5d: chunk->data    = ", rank);
			
			for (int k = 0; k < chunk->index_data.capacity; k++) {
				printf("%d ", chunk->data[k]);
			}
			
			printf("\n");
		}
	}
	
}






