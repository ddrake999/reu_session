#!/usr/bin/bash

#!/bin/bash
#SBATCH --job-name hello_mpi
#SBATCH --nodes 2
#SBATCH --ntasks-per-node 4
#SBATCH --partition short
#SBATCH --output output.txt

mpirun -n 8 ./hello_mpi
