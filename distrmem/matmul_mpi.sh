#!/usr/bin/bash

#SBATCH --job-name matmul_mpi
#SBATCH --nodes 2
#SBATCH --ntasks-per-node 20
#SBATCH --oversubscribe
#SBATCH --distribution cyclic
#SBATCH --partition allcpu
#SBATCH --output output.txt

# Compile the program.
mpicc -std=c99 -o matmul_mpi matmul_mpi.c

# Run the program with various process counts.
for nprocs in 1 2 4 8 16 32 64 128
do
	mpirun -n $nprocs ./matmul_mpi 1024 R
done
