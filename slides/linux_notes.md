# Linux Notes
POSIX Portable Unix - POSIX standard 1988, 3 years before LT started developing Linux 

Many Linux distributions, some better for servers others for PC

- RPM-based: CentOS, Red Hat, Open SUSE
- Debian-based: Debian, Ubuntu, Linux Mint
- Pacman
- Gentoo
- Slackware
- Independent - e.g. Tiny Core Linux 11MB or with GUI 16MB

OSX follows POSIX too and also uses Bash interpreter.  Linux uses monolithic kernel vs. microkernel OSX uses compromise architecture

Command Line vs GUI
- With a GUI we don't need to memorize so much.
- With command line we can automate much more easily.

Don't try to memorize all at once - make cheat sheets

Open a terminal 

- Ctrl + Alt + T on Linux
- Command + Space "terminal" on OSX
- Windows is not a POSIX OS.  Install Cygwin or better VirtualBox and a Linux VM

## Navigating at the command line
### The command prompt
user@machine:current path $

### Keyboard shortcuts
- Ctrl + A	Go to the beginning of the line you are currently typing on
- Ctrl + E	Go to the end of the line you are currently typing on
- Ctrl + L	Clears the Screen, similar to the clear command
- Ctrl + U	Clears the line before the cursor position. If you are at the end of the line, clears the entire line.
- Ctrl + R	Let's you search through previously used commands
- Ctrl + C	Kill whatever you are running (NOT copy to clipboard)
- Ctrl + D	Exit the current shell
- Ctrl + Z	Puts whatever you are running into a suspended background process. fg restores it. (more on this later)
- Ctrl + W	Delete the word before the cursor
- Ctrl + K	Clear the line after the cursor
- Ctrl + Y  Paste deleted word or line
- Alt + F	Move cursor forward one word on the current line
- Alt + B	Move cursor backward one word on the current line
- Tab	Auto-complete files and folder names
- Ctrl + Shift + C  Copy selected text to global clipboard
- Ctrl + Shift + V  Paste selected text from global clipboard

### More on this
- Using the command buffer (arrow up for previous command)
- Using history command (specify a command like this: !23

## Getting information about the system
- uname -a  (information about your kernel and operating system distro)
- systemd-analyze time (get information about boot time)
- whoami (currently logged in user.  Can use su to impersonate another user) 
- who (who is logged onto the server, lots of other info - check man pages)
- hostnamectl (more info about the machine/os
- cal (a text-based calendar)
- date (the current date/time)
- which (show the path to a command, e.g. which python3)
- whereis (locates binary, source and manual files for the specified command names)
- du (disk usage)
- df (disk free space)
- lsblk (info about block storage devices usb, flash...)
- lscpu (info about the processor)

## Navigating the file system
The file system is a tree structure rooted at /
Viewing and understanding ownership, permissions, hidden files, hard and soft links.  Special directories . and .. and ~

Have a look at the root directories 

- pwd (print working directory -- full path to current directory)
- ls -a -l -R -r -t -S -h  (list files and subdirectories of the specified directory, defaulting to the current directory.  Example ls -ltr lists newest modifications at the end.)
- stat (display file of file system status of the specified file or directory)
- cd (change directory)
- man (manual pages for a command)
- cat (usually used to display the full contents of a file, can be used to concatenate text files)
- tail (show the last few lines of a text file)
- more (show the first page of a text file with simple navigation)
- tree (show a tree diagram of the specified directory)
- grep (search through text files for a keyword)
- find -name -mtime -user (find files by name or modification time...)
- diff
- wc (word count - lines words chars)

## Working with files and directories
Learn about hidden files, hard and soft links, ownership and permissions, redirecting input and output with >, >> and < pipe | and tee

- touch (create an empty file or update the modification date of an existing one)
- rm (remove a file permanently, recursive option to remove whole directory trees)
- mkdir (create a directory)
- rmdir (remove an empty directory)
- cp (copy a file or files, recursive option to copy a directory)
- mv (move or rename a file or directory)
- echo (echo out some text e.g. -e "dog\ncat\nzebra\nmouse" > temp
- sort (sort a text file e.g. sort < temp > temp1)
- ln (create a hard link to a file or directory, ln -s creates a soft or symlink learn how these work...)
- text editors: nano, vim, emacs
- diff (display the differences between two text files)
- cat /dev/null > temp (/dev/null is the null output device (bit bucket) send unwanted output there.)

### /sys virtual filesystem
One of the interesting aspects of Linux systems is the virtual sys file system.  You can use normal file system commands like cd, cat, ls, etc to make changes at the hardware level (e.g. turn on an LED on your keyboard).

## Working with processes
- stdout = 1 standard output stream
- stderr = 2 standard error output stream
- /dev/null null output stream (the bit bucket)
- & after redirect indicates that what follows is a descriptor not a file name
- ps (list current processes -- see man pages for more info)
- top (a dynamic display of the current processes by CPU usage (like system monitor)
- Ctrl + Z (stop the current process and put it in the background)
- fg (bring a background process back to the foreground)
- jobs (list all background processes)
- time (calculate how long it takes to execute a command)

## Profile and scripting 
- .bashrc .bash_profile (make changes to .bashrc to set your default command line environment (path, other variables)
- easy to automate repetitive tasks (just make a text file with the commands)  For example, a script to pull changes from a git repository push to another, compile and run tests.
- sourcing a script (the . command) runs the commands in the script in the current process
- making a script executable and executing it as a new process (chmod +x)
- shebang line e.g. #!/usr/env/python3 tells the operating system what program you want to execute the script with

## Environment
- env (list all environment variables)
- PATH (locations for executable files.  If a command is in the path, can run it without ./
- LD_LIBRARY_PATH (directories to be searched first for shared library files)
- PYTHONPATH (augment the search path for Python modules)


## Administration - Ownership of and permissions on files and directories
- sudo (execute a command with root privileges
- su (impersonate another user)
- cat etc/group (get info about groups)
- groups (which groups do I belong to?)
- chmod u, g, o, a; r, w, x, s (setuid/setgid bit), t (sticky bit) (set file and directory permissions) 
- octal bit masks
- chown can change owner and group at the same time
- chgrp to change group only
- can use these recursively (careful!)
- adduser (create a new user -- good to experiment with on your own system to understand permissions better)
- deluser --remove-home (remove the specified user and their home directory)
- mount (attach file system on a physical disk (device) to file system tree)
  for example, attach an SD card or thumb drive at a directory in /media/

### PC vs Server - What am I allowed to do on the server?
When working on a server, you do not have sudo privileges so you may be limited when it comes to installing applications you need.  You can't do much outside of your own home directory.  A system administrator may be able to give some more detailed guidelines.  When working on a server, we may need to know how to build applications from source.

## Networking
- ssh (create a secure shell connection to a server)
- scp (securely copy files to or from a servrer)
- Filezilla, Putty (applications you can install to perform these actions)

## Applications - Building, Installing, Debugging Installation
- apt (package manager for installing applications to your system)
- pip, pip3 (python package manager for managing Python modules like numpy, scipy)
- wget (fetch the contents of a URL, usually used to download a file from the command line)
- tar zvf (extract the contents of a compressed .tar.gz file
- git clone, pull, checkout, etc (git is the standard for distributed version control.  Learn how to use it.  There is an online tutorial at github.
- md5sum (compute the checksum of a downloaded file.  This ensures that the file was not corrupted or tampered with)
- make (used to execute a makefile, which contains instructions for building an application from source)
- cmake (configure) (cmake is a complex application for constructing make files that are platform independent)
- which (see above)
- whereis (see above)
- ldd (print a shared libraries dependencies -- helpful in debugging an install)
- env (check for missing environment variables)
