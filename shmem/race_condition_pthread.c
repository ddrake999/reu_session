#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

//
// Section for global variables, specifically for
// the mutex objects needed for updating critical
// sections of code.
//

// A lock to protect the update of the sum.
pthread_mutex_t sum_lock;

// The global sum we plan to update.
unsigned global_sum = 0;

// The number of threads.
int num_threads;	

// The worker function used by each thread.
void *worker(void *arg);

int main(int argc, char *argv[])
{
	if (argc > 1) {
		num_threads = strtol(argv[1], NULL, 10);
		
		if (num_threads <= 0) {
			fprintf(stderr, "\a\aUsage:\n\trace_condition_pthread <N>, where N is the number of natural numbers to sum.\n");
			
			return 0;
		}
	} else {
		num_threads = 4;
	}
	
	// Allocate space for the thread ids we will assign to each thread.
	unsigned *thread_ids = malloc(num_threads * sizeof(int));
	
	if (NULL == thread_ids) {
		fprintf(stderr, "Could not dynamically allocate array thread_ids of size %d, exiting now.\n", num_threads);
		
		return 1;
	}
	
	// Allocate space for the actual opaque pthread_t data type.
	pthread_t *threads = malloc(num_threads * sizeof(pthread_t));
	
	if (NULL == threads) {
		fprintf(stderr, "Could not dynamically allocate array threads of size %d, exiting now.\n", num_threads);
		free(thread_ids);
		
		return 1;
	}
	
	// Initialize the lock.
	pthread_mutex_init(&sum_lock, NULL);
	
	// Initialize the threads and run
	for (unsigned i = 0; i < num_threads; i++) {
		thread_ids[i] = i;
		
		int create_status = pthread_create(&threads[i], NULL, worker, &thread_ids[i]);
		
		if (create_status != 0) {
			fprintf(stderr, "Could not allocate additional threads, joining the first %d created threads and exiting.\n", i);
			
			// Join the remaining threads and exit.
			for (int j = i - 1; j >= 0; j--) {
				pthread_join(threads[j], NULL);
			}
			
			// Now free the resources we are no longer using.
			pthread_mutex_destroy(&sum_lock);
			free(thread_ids);
			free(threads);
			
			return 1;
		}
	}
	
	for (unsigned i = 0; i < num_threads; i++) {
		pthread_join(threads[i], NULL);
	}
	
	// Free the resources held by the mutex.
	pthread_mutex_destroy(&sum_lock);
	
	// Display the computed sum to the user.
	printf("Number of threads:  %d\n", num_threads);
	printf("Sum using pthreads: %u\n", global_sum);
	printf("The actual sum:     %d\n\n", num_threads);
	
	// Free the memory we no longer need.
	free(thread_ids);
	free(threads);
	
	return 0;
}

// Worker function that increments the global sum.
void *worker(void *arg)
{
	// pthread_mutex_lock(&sum_lock);
	global_sum++;
	// pthread_mutex_unlock(&sum_lock);

	return NULL;
}



























