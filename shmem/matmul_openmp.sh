#!/usr/bin/bash
#SBATCH --job-name mmul_omp_perf
#SBATCH --nodes 1
#SBATCH --partition short
#SBATCH --output output.txt

gcc -fopenmp -o matmul_openmp matmul_openmp.c

for i in 1 2 4 8 16 32 64 128
do
	# Set the number of OpenMP threads to use.
	export OMP_NUM_THREADS=$i

	# Run the program.
	./matmul_openmp 1024
done
