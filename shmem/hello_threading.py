#!/usr/bin/python3
#
# hello_threading.py
#
# A Python script that uses the threading module to greet the user.

import threading
import sys

# Create a worker function to greet the user.
def worker(thread_id, total_threads):
	print("Hello from thread {0} of {1} threads".format(thread_id, total_threads))

if __name__ == '__main__':
	# Set the number of threads based on user input.
	if len(sys.argv) > 1:
		try:
			num_threads = int(sys.argv[1])
		except ValueError:
			print("Second argument needs to be a valid base-10 number.")
			print("Using the default thread count of 4 threads.")
			
			num_threads = 4
	else:
		num_threads = 4

	# Create a thread pool (a team of threads).
	thread_pool = []
	for k in range(num_threads):
		thread_pool.append(threading.Thread(target = worker, args = (k, num_threads)))
	
	for thread in thread_pool:
		thread.start()
		
	for thread in thread_pool:
		thread.join()