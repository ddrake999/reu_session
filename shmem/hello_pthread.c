#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

// The worker function used by each thread.
void *worker(void *arg);

int main(int argc, char *argv[])
{
	int num_threads;
	
	if (argc > 1) {
		num_threads = strtol(argv[1], NULL, 10);
		
		if (num_threads <= 0) {
			fprintf(stderr, "\a\aUsage:\n\thello_pthread <N>, where N is the number of threads.\n");
			
			return 0;
		}
	} else {
		num_threads = 4;
	}
	
	int *thread_ids = malloc(num_threads * sizeof(int));
	pthread_t *threads = malloc(num_threads * sizeof(pthread_t));
	
	for (int i = 0; i < num_threads; i++) {
		thread_ids[i] = i;
		
		pthread_create(&threads[i], NULL, worker, &thread_ids[i]);
	}
	
	for (int i = 0; i < num_threads; i++) {
		pthread_join(threads[i], NULL);
	}
	
	free(thread_ids);
	free(threads);
	
	return 0;
}

void *worker(void *arg)
{
	int thread_id = *(int *)arg;
	
	printf("Hello world from thread %d!\n", thread_id);
	
	return NULL;
}
