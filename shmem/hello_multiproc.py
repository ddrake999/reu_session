# hello_multiproc.py
#
# A Python script that uses the multiprocessing module to greet the user.

import multiprocessing
import sys

# Create a worker function to greet the user.
def worker(process_id, total_ptocesses):
	print("Hello from process {0} of {1} processes".format(process_id, total_ptocesses))

if __name__ == '__main__':
	# Set the number of threads based on user input.
	if len(sys.argv) > 1:
		try:
			num_processes = int(sys.argv[1])
		except ValueError:
			print("Second argument needs to be a valid base-10 number.")
			print("Using the default process count of 4 processes.")
			
			num_processes = 4
	else:
		num_processes = 4

	# Create a pool of processes and run the worker function.
	with multiprocessing.Pool(num_processes) as pool:
		# Run the worker greeting.
		pool.starmap(worker, [(k, num_processes) for k in range(num_processes)])
