#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/time.h>

// The number of seconds in a microsecond, used
// to time our matrix multiplication method.
#define MICROSECONDS_PER_SECOND 1e-6

// A struct for sharing the matrices among the threads.
typedef struct matprod_s
{
	int thread_id;
	int num_threads;
	double **A, **B, **C;
	int N;
} matprod;

// The worker function used by each thread.
void *worker(void *arg);

// matalloc()
//
// Function that allocates a matrix of dimension NxN.
double **matalloc(int N);

// matfree()
//
// Frees the memory held by a matrix.
double matfree(double ***matrix, int N);

// eye()
//
// Creates an identity matrix.
void eye(double **A, int N);

// laplacian()
//
// Creates the laplacian matrix.
void laplacian(double **A, int N);

// randmat()
//
// Creates a random matrix.
void randmat(double **A, int N, long seed);

// print_matrices()
//
// Prints the matrices we multiplied, along with their product.
void print_matrices(double **A, double **B, double **C, int N);

int main(int argc, char *argv[])
{
	int N;
	int num_threads;
	double **A = NULL, **B = NULL, **C = NULL;
	struct timeval start, end;
	double elapsed;
	
	if (argc > 1) {
		N = strtol(argv[1], NULL, 10);
		
		if (N <= 0) {
			fprintf(stderr, "\a\aUsage:\n\tmatmul_pthread <N> <P>, where N is the dimension and P is the number of threads.\n");
			
			return 0;
		}
	} else {
		fprintf(stderr, "\a\aUsage:\n\tmatmul_pthread <N> <P>, where N is the dimension and P is the number of threads.\n");
		
		return 1;
	}
	
	if (argc > 2) {
		num_threads = strtol(argv[2], NULL, 10);
		
		if (num_threads <= 0) {
			fprintf(stderr, "\a\aUsage:\n\tmatmul_pthread <N> <P>, where N is the dimension and P is the number of threads.\n");
			
			return 1;
		}
	} else {
		num_threads = 1;
	}
	
	matprod *products = malloc(num_threads * sizeof(matprod));
	pthread_t *threads = malloc(num_threads * sizeof(pthread_t));
	
	// Allocate the matrices.
	A = matalloc(N);
	B = matalloc(N);
	C = matalloc(N);
	
	// Seed the random number generator.
	srand48(time(NULL));
	
	// Initialize the matrices A and B.
	laplacian(A, N);
	laplacian(B, N);
	
	gettimeofday(&start, NULL);
	for (int i = 0; i < num_threads; i++) {
		// Initialize each portion of the product we will be computing.
		products[i].thread_id = i;
		products[i].num_threads = num_threads;
		products[i].A = A;
		products[i].B = B;
		products[i].C = C;
		products[i].N = N;
		
		// Let each thread get to work.
		pthread_create(&threads[i], NULL, worker, &products[i]);
	}
	
	for (int i = 0; i < num_threads; i++) {
		pthread_join(threads[i], NULL);
	}
	gettimeofday(&end, NULL);
	
	// Compute the elapsed time it took to multiply the matrices.
	elapsed = (double)(end.tv_sec - start.tv_sec) + ((double)(end.tv_usec - start.tv_usec)) * MICROSECONDS_PER_SECOND;
	printf("Elapsed time for multiplying two %dx%d matrices: %f seconds\n", N, N, elapsed);
	
	// Print the matrices and the ouput.
	if (N < 4) {
		print_matrices(A, B, C, N);
	}
	
	// Free the matrices.
	matfree(&A, N);
	matfree(&B, N);
	matfree(&C, N);
	
	free(products);
	free(threads);
	
	return 0;
}

void *worker(void *arg)
{
	matprod *product = (matprod *)arg;
	int id = product->thread_id;
	int num_threads = product->num_threads;
	double **A = product->A;
	double **B = product->B;
	double **C = product->C;
	int N = product->N;
	
	int starti = id * (N / num_threads);
	int endi;
	
	if (id < num_threads - 1) {
		endi = starti + (N / num_threads);
	} else {
		endi = N;
	}
	
	// printf("Thread %d multipling matrices A and B between rows %d and %d.\n", id, starti, endi);
	
	for (int i = starti; i < endi; i++) {
		for (int j = 0; j < N; j++) {
			for (int k = 0; k < N; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}
	
	// printf("Thread %d ending.\n", id);
	
	return NULL;
}

// matalloc()
//
// Function that allocates a matrix of dimension NxN.
double **matalloc(int N)
{
	double **matrix = malloc(N * sizeof(double *));
	int i, j;
	
	for (i = 0; i < N; i++) {
		matrix[i] = malloc(N * sizeof(double));
		
		for (j = 0; j < N; j++) {
			matrix[i][j] = 0.0;
		}		
	}
	
	return matrix;
}

// matfree()
//
// Frees the memory held by a matrix.
double matfree(double ***matrix, int N)
{
	int i;
	
	for (i = 0; i < N; i++) {
		free((*matrix)[i]);
	}
	
	free(*matrix);
	*matrix = NULL;
}

// eye()
//
// Creates an identity matrix.
void eye(double **A, int N)
{
	int i;
	
	for (i = 0; i < N; i++) {
		A[i][i] = 1.0;
	}
}

// laplacian()
//
// Creates the laplacian matrix.
void laplacian(double **A, int N)
{
	int i;
	
	for (i = 0; i < N - 1; i++) {
		A[i][i] = 2.0;
		A[i][i + 1] = -1.0;
		A[i + 1][i] = -1.0;
	}
	
	A[N - 1][N - 1] = 2.0;
}

// randmat()
//
// Creates a random matrix.
void randmat(double **A, int N, long seed)
{
	int i, j;
	
	// Initialize the matrix.
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			A[i][j] = drand48();
		}
	}
}

// print_matrices()
//
// Prints the matrices we multiplied, along with their product.
void print_matrices(double **A, double **B, double **C, int N)
{
	int i, j;
	
	printf("A = \n");
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%5.2f ", A[i][j]);
		}
		
		printf("\n");
	}
	printf("\n");
	
	printf("B = \n");
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%5.2f ", B[i][j]);
		}
		
		printf("\n");
	}
	printf("\n");
	
	printf("C = \n");
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%5.2f ", C[i][j]);
		}
		
		printf("\n");
	}
	printf("\n");
}