Documentation for running the hello world programs
===================================================

The Hello World Programs

	For hello_threading.py:
		python3 hello_threading.py N
		
		Notes:
			N is an optional argument indicating the number of threads. By default,
			the number of threads used is 4.
			
	For hello_multiproc.py:
		python3 hello_multiproc.py N
		
		Notes:
			N is an optional argument indicating the number of threads. By default,
			the number of threads used is 4.
			
	For hello_openmp.c:

		1. Compile the program
			gcc -fopenmp -o hello_openmp hello_openmp.c
			
		2. Specify the thread count at the command line.
			export OMP_NUM_THREADS=N
			
			a) Here, N is an integer number of threads greater than or equal to 1.
			
		3. Run the program.
			./hello_openmp
			
	For hello_pthread.c:
		
		1. Compile the program
			gcc -pthread -o hello_pthread hello_pthread.c
			
		2. Run the program.
			./hello_pthread <P>
			
			a) Here, P is the number of threads.
			
The Matrix Multiplication Programs

	For matmul_openmp.c:

		1. Compile the program
			gcc -fopenmp -o matmul_openmp matmul_openmp.c
			
		2. Specify the thread count at the command line.
			export OMP_NUM_THREADS=P
			
			a) Here, P is an integer number of threads greater than or equal to 1.
			
		3. Run the program.
			./matmul_openmp <N>
			
			a) N is the number of rows and columns.
			
	For matmul_pthread.c:

		1. Compile the program
			gcc -pthread -o matmul_pthread matmul_pthread.c
			
		2. Run the program.
			./matmul_pthread <N> <P>
			
			a) N is the dimension and P is the number of threads with P <= N.

The Race Condition Program
	
	For race_condition_pthread.c
		
		1. Compile the program
			gcc -pthread -o race_condition_pthread race_condition_pthread.c

		2. Run the program
			./race_condition_pthread.sh

#-----------------------------------------------------------------------------#






























