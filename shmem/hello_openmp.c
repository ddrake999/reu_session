#include <stdio.h>
#include <omp.h>

int main(void)
{
	int thread_id;
	int total_threads;
	
	#pragma omp parallel private(thread_id, total_threads)
	{
		// Have each thread get the total number of threads in the team.
		total_threads = omp_get_num_threads();
		
		// Have each thread get its individual thread ID.
		thread_id = omp_get_thread_num();
		
		// Greet the user.
		printf("Hello from %3d of %3d threads.\n", thread_id, total_threads);
	}
	
	return 0;
}