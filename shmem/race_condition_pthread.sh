#!/usr/bin/bash
#SBATCH --job-name race_condition_pthread
#SBATCH --nodes 1
#SBATCH --ntasks-per-node 1
#SBATCH --partition short
#SBATCH --output output.txt

gcc -pthread -o race_condition_pthread race_condition_pthread.c

for i in {1..10}
do
    ./race_condition_pthread $((i*1000))
done
