/*
 * matmul_openmp.c
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <omp.h>

#define MICROSECONDS_PER_SECOND 1e-6

// matmul()
//
// Function that multiplies two matrices.
void matmul(double **C, double **A, double **B, int N)
{
	int i, j, k;
	
	#pragma omp parallel for private(i, j, k) shared(A, B, C, N) collapse(3)
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			for (k = 0; k < N; k++) {
				C[i][j] += A[i][k] * B[k][j];
			}
		}
	}
	
}

// matalloc()
//
// Function that allocates a matrix of dimension NxN.
double **matalloc(int N)
{
	double **matrix = malloc(N * sizeof(double *));
	int i, j;
	
	for (i = 0; i < N; i++) {
		matrix[i] = malloc(N * sizeof(double));
		
		for (j = 0; j < N; j++) {
			matrix[i][j] = 0.0;
		}		
	}
	
	return matrix;
}

// matfree()
//
// Frees the memory held by a matrix.
double matfree(double ***matrix, int N)
{
	int i;
	
	for (i = 0; i < N; i++) {
		free((*matrix)[i]);
	}
	
	free(*matrix);
	*matrix = NULL;
}

// eye()
//
// Creates an identity matrix.
void eye(double **A, int N)
{
	int i;
	
	for (i = 0; i < N; i++) {
		A[i][i] = 1.0;
	}
}

// laplacian()
//
// Creates the laplacian matrix.
void laplacian(double **A, int N)
{
	int i;
	
	for (i = 0; i < N - 1; i++) {
		A[i][i] = 2.0;
		A[i][i + 1] = -1.0;
		A[i + 1][i] = -1.0;
	}
	
	A[N - 1][N - 1] = 2.0;
}

// randmat()
//
// Creates a random matrix.
void randmat(double **A, int N, long seed)
{
	int i, j;
	
	// Initialize the matrix.
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			A[i][j] = drand48();
		}
	}
}

// print_matrices()
//
// Prints the matrices we multiplied, along with their product.
void print_matrices(double **A, double **B, double **C, int N)
{
	int i, j;
	
	printf("A = \n");
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%5.2f ", A[i][j]);
		}
		
		printf("\n");
	}
	printf("\n");
	
	printf("B = \n");
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%5.2f ", B[i][j]);
		}
		
		printf("\n");
	}
	printf("\n");
	
	printf("C = \n");
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%5.2f ", C[i][j]);
		}
		
		printf("\n");
	}
	printf("\n");
}

int main(int argc, char *argv[])
{
	int N;
	int num_threads;
	double **A = NULL, **B = NULL, **C = NULL;
	struct timeval start, end;
	double elapsed;
	
	if (argc > 1) {
		N = strtol(argv[1], NULL, 10);
		
		if (N <= 0) {
			fprintf(stderr, "\a\aUsage:\n\tmatmul_openmp <N> <P>, where N is the number of rows and columns and P is the number of threads.\n");
			
			return 0;
		}
	} else {
		N = 4;
	}

	if (argc > 2) {
		num_threads = strtol(argv[2], NULL, 10);
		
		if (num_threads <= 0) {		
			fprintf(stderr, "\a\aUsage:\n\tmatmul_openmp <N> <P>, where N is the number of rows and columns and P is the number of threads.\n");

			return 0;
		} else {
			omp_set_num_threads(num_threads);
		}
	}
	
	
	// Allocate the matrices.
	A = matalloc(N);
	B = matalloc(N);
	C = matalloc(N);
	
	// Seed the random number generator.
	srand48(time(NULL));
	
	// Initialize the matrices A and B.
	randmat(A, N, 0);
	randmat(B, N, 0);
	
	// Multiply the matrices.
	gettimeofday(&start, NULL);
	matmul(C, A, B, N);
	gettimeofday(&end, NULL);
	
	// Compute the elapsed time it took to multiply the matrices.
	elapsed = (double)(end.tv_sec - start.tv_sec) + ((double)(end.tv_usec - start.tv_usec)) * MICROSECONDS_PER_SECOND;
	
	#pragma omp parallel shared(N, elapsed)
	{
		#pragma omp master
		printf("Elapsed time for multiplying two %dx%d matrices with %d threads: %f seconds\n", N, N, omp_get_num_threads(), elapsed);
	}
	// Print the matrices and the ouput.
	if (N < 4) {
		print_matrices(A, B, C, N);
	}
	
	// Free the matrices.
	matfree(&A, N);
	matfree(&B, N);
	matfree(&C, N);
	
	return 0;
}

























